# Nodemcu #

# Modeules #

You successfully commissioned a NodeMCU custom build from the dev branch. You selected the following 24 modules: 

1. adc
2. am2320
3. bit
4. bme280
5. crypto
6. dht
7. file
8. gpio
9. hmc5883l
10. http
11. i2c
12. mqtt
13. net
14. node
15. ow (one-wire)
16. pcm
17. pwm
18. sjson
19. spi
20. tmr (timer)
21. uart
22. websocket
23. wifi
24. ws2812

