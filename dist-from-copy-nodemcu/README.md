# Firmware para distribuir

Se trata de una copia del firmware lista para instalar en las
tarjetas de desarrollo nodemcu con memoria igual a 4MB;
las placas que cumplen con estas características son las nodemcu V3.

## Lista de firmware

```bash
.
├── bitacora.csv
├── catalejo-nodemcu-25-03-16-14-16-54.bin.tar.gz # versión 2025-03-16
├── catalejo-firmware-4MB-2023-01-20-blink-test.tar.gz # versión con borrado de run.lua a través de botón flash
├── catalejo-firmware-4MB-blink-test.tar.gz # versión usada en el 2018 a 2022
├── catalejo-firmware-4MB.tar.gz # Versión inicial, será borrada ya que es la misma versión usada en el 2018 al 2022
└── README.md
```

* `catalejo-nodemcu-25-03-16-14-16-54.bin.tar.gz`: Version 2025-03-16 para nodemcu con 4MB, código de prueba con blink.
* `catalejo-firmware-4MB-2023-01-20-blink-test.tar.gz`: Se trata del firmware listo para correr en el nodemcu v3, pero con una prueba de parpadeo del LED en el
pin 4; esta instalación y test es útil cuando se requiere instalar muchas placas al tiempo y queremos optimizar en el proceso de pruebas de
una correcta instalación.
* `catalejo-firmware-4MB.tar.gz`: firmware listo para correr en el nodemcu v3 2018 al 2022

## Subir firmware

Aquí se hace de algunos de los recomendados por espressif, como puede ser esptool.

Para la instalación del esptool se puede hacer desde el comando `pip install esptool`;
cuando estas dependencias fallan, se puede hacer uso desde los [releases de esptool](https://github.com/espressif/esptool/releases)
dando permisos de ejecución a los binarios.

Ejemplo para subir el firmware en un esp8266 e 12f con 4MB de memoria
```bash
esptool --port /dev/ttyUSB0 write_flash -fm dio 0x00000 catalejo-2023-01-20.bin
```

## Observaciones

* Se requiere generar un firmware .bin para el esp8266 e07 de 1MB, el firmware ofrecido
a la fecha no se puede subir allí.

* El firmware requiere ser modificado para que permita subir además de scripts .lua
binarios de cualquier tipo, como también borrarlos.

* 2023-04-06 se realizan pruebas para el modo wifi sta/ap simultaneo, sin embargo, cuando el esp8266
busca conectarse como sta afecta el rendimiento del mismo para otras tareas, por tanto, es mejor
disminuir la cantidad de intentos de conexión TODO: borrar cuando esto sea solucionado.

## Herramienta para subir el firmware.bin

```bash
pip install esptool
```
