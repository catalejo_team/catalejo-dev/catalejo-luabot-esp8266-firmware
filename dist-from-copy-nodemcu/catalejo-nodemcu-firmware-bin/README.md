Los archivos ./0x00000.bin y ./0x10000.bin han sido compilados con nodemcu-firware para el esp8266.

Estos archivos se suben al esp8266 con el comando:

```bash
esptool.py write_flash 0x00000 0x00000.bin 0x10000 0x10000.bin
```

Además, se requiere de los scripst cross-compilados con luac que se encuentran en
`../../scripts/luabot/lua/upload/` los cuales se suben desde la carpeta `../../scripts/luabot/lua/` con el comando:

```bash
make mk-upload-folder upload-folder
```

## Dependencias

### Cross compilador de luac.cross

El crosscompilador de lua para la arquitectura del esp8266 se puede enlazar a través del siguiente comando:

```bash
ln -sr ./luac.cross ~/.local/bin/
```

### ESPTOOL

Para ver los comandos de instalación revise el archivo `./go`.

## Observaciones

2025-03-16 Por favor, crear una versión de esta documentación que mantenga las instrucciones de croscompilación desde catalejo-nodemcu
como se ha hecho en los ejemplos del catalejo-micropython-esp32-c3-mini

