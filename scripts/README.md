# Lua scripts


## Instalar herramienta en python:

Se recomienda instalar miniconda para manejar las
variables de entorno de python.

Después de instalar conda puede hacer los siguiente:

```
conda create --name nodemcu # Seguir los pasos indicados
conda activate nodemcu
conda activate pip
```

Dentro de la variable de entorno activada ejecute los siguientes comandos:

```bash
pip install pyserial
pip install nodemcu-uploader
```

## Ejemplos de uso:

* Subir script:

```bash
nodemcu-uploader upload init.lua
nodemcu-uploader download wifi.lc
```
