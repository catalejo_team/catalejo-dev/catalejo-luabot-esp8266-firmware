dofile("hcsr04.lc")

sensor = HCSR04.new(1, 2)

mytimer = tmr.create()
mytimer:register(1000, tmr.ALARM_AUTO, function()
  print(sensor.status())
  if sensor.status() == 0 then
    print("distancia: "..sensor.measure())
  end
  if sensor.status() < 1 then
    sensor.trigger()
  end
end)
mytimer:start()
