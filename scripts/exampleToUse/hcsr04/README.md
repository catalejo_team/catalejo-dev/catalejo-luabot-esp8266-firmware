# Ultrasonido hcsr-04

## Ejemplo simple

```lua
dofile("hcsr04.lua")

sensor = HCSR04.new(1, 2)

mytimer = tmr.create()
mytimer:register(1000, tmr.ALARM_AUTO, function()
  print(sensor.status())
  if sensor.status() == 0 then
    print("distancia: "..sensor.measure())
  end
  if sensor.status() < 1 then
    sensor.trigger()
  end
end)
mytimer:start()
```

## Funciones

* `HCSR04.new(trigger, echo)`: Instancia el sensor según pines correspondientes.
* `HCSR04.activate()`: Activar el sensor ultrasonido.
* `HCSR04.deactivate()`: Detiene las interrupciones asociadas al sensor ultrasonido
* `HCSR04.trigger()`: Inicia lectura del sensor
* `HCSR04.measure()`: Obtención de la medida en milimetros
* `HCSR04.status()`: Reporta el estado de la medición;

|Estado|Descripción|
|:-------------:|:-------------:|
|-2| Sensor activado|
|-1| Error en la lectura|
|0| Tarea completada|
|1| Lectura en proceso|
|2| Sensor desactivado|

## Observaciones

No siempre la medida es coherente y requiere de un método estadístico para aislar los errores y mejor
la exactitud de los datos.

Realizado por:

Johnny Cubides
