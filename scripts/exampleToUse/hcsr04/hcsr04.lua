HCSR04 = {}

HCSR04.new = function (trig, echo)
  local self = {}
  -- 0 = succesful, 1 = en proceso, -1 = error, -2 = activate, 2 = desactivado
  local t_rising, t_falling
  local status = 2 -- desactivado

  local function cb_echo(level, when)
    if level == gpio.HIGH then
      t_rising = when
    elseif level == gpio.LOW then
      t_falling = when
      if t_falling > t_rising then
        status = 0 -- succesful
      else
        status = -1 -- error
      end
    end
  end

  function self.activate()
    gpio.mode(trig, gpio.OUTPUT)
    gpio.write(trig, gpio.LOW)
    gpio.mode(echo, gpio.INT)
    gpio.trig(echo, "both", cb_echo)
    status = -2 -- activate
  end

  function self.deactivate()
    gpio.trig(echo, "none")
    status = 2 -- desactivado
  end

  function self.status()
    return status
  end

  function self.trigger()
    t_rising, t_falling, status = 0, 0, 1 -- en proceso
    gpio.write(trig, gpio.HIGH)
    tmr.delay(10)
    gpio.write(trig, gpio.LOW)
  end

  function self.measure()
     -- 340/2 = 170
    return (t_falling - t_rising) * 170 / 1000 -- centimetros
  end

  self.activate()

  return self
end
