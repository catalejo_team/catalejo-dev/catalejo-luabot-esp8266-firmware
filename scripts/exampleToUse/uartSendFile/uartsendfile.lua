-- Configura el puerto serial
uart.setup(0, 115200, 8, uart.PARITY_NONE, uart.STOPBITS_1, 0)

-- Variables de estado
local buffer = ""
local fileName = ""
local fileContent = ""
local fileNameLength = 0
local fileContentLength = 0
local receivedContentLength = 0
local state = 0
local fileHandle = nil

-- Función para inicializar el archivo
local function initFile(fileName)
	if file.open(fileName, "w+") then
		file.close()
		return true
	else
		print("Error al abrir el archivo para escribir")
		return false
	end
end

-- Función para escribir una parte del archivo
local function writeFilePart(fileName, content)
	if file.open(fileName, "a+") then
		file.write(content)
		file.close()
	else
		print("Error al abrir el archivo para escribir")
	end
end

uart.on("data", 1, function(data)
	buffer = buffer .. data

	if state == 0 then
		-- Leer la longitud del nombre del archivo (2 bytes)
		if #buffer >= 2 then
			fileNameLength = string.byte(buffer, 1) * 256 + string.byte(buffer, 2)
			buffer = string.sub(buffer, 3)
			state = 1
		end
	end

	if state == 1 then
		-- Leer el nombre del archivo
		if #buffer >= fileNameLength then
			fileName = string.sub(buffer, 1, fileNameLength)
			buffer = string.sub(buffer, fileNameLength + 1)
			if initFile(fileName) then
				state = 2
			else
				state = 0
			end
		end
	end

	if state == 2 then
		-- Leer la longitud del contenido del archivo (4 bytes)
		if #buffer >= 4 then
			fileContentLength = string.byte(buffer, 1) * 16777216
				+ string.byte(buffer, 2) * 65536
				+ string.byte(buffer, 3) * 256
				+ string.byte(buffer, 4)
			buffer = string.sub(buffer, 5)
			receivedContentLength = 0
			state = 3
		end
	end

	if state == 3 then
		-- Leer el contenido del archivo en partes
		local remainingLength = fileContentLength - receivedContentLength
		local dataToWrite = string.sub(buffer, 1, remainingLength)
		writeFilePart(fileName, dataToWrite)
		receivedContentLength = receivedContentLength + #dataToWrite
		buffer = string.sub(buffer, #dataToWrite + 1)

		if receivedContentLength >= fileContentLength then
			print("Archivo guardado exitosamente:")
			print("Nombre del archivo: " .. fileName)
			state = 0
		end
	end

	-- Enviar confirmación
	uart.write(0, ">\r\n")
end, 0)

uart.write(0, ">\r\n")
