-- En este ejemplo se imprime una cuenta
-- en periodos de 1 segundo por timer
local counter = 0
mytimer = tmr.create()
mytimer:register(1000, tmr.ALARM_AUTO, function()
  print("cuenta: "..counter)
  counter = counter + 1
end)
mytimer:start()
