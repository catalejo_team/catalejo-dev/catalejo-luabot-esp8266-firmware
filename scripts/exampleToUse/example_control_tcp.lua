miControl = net.createServer(net.TCP, 30)

function cb_miControl(sck, data)
  print(data)
end

if miControl then
  miControl:listen(3000, function(conn)
   conn:on("receive", cb_miControl)
 end)
end
