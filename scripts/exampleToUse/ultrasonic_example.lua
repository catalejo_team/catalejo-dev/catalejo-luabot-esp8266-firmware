----ultra[1] = trig
----ultra[2] = echo
--t_start = 0
--t_end = 0

--trig = 0
--echo = 0

--function readUltrasonic(u)
--  trig = u[1]
--  echo = u[2]
--  gpio.mode(trig, gpio.OUTPUT)
--  gpio.mode(echo, gpio.INT)
--  gpio.write(trig, gpio.HIGH)
--  tmr.delay(10)
--  gpio.write(trig, gpio.LOW)
--  gpio.trig(echo, "up", cb_ultrasonic)
--  tmr.delay(12000)
--  if (t_end - t_start) < 0 then
--    return -1
--  end
--  return (t_end - t_start) / 58
--end

--local function cb_ultrasonic(level, when)
--  if level == 1 then
--    t_start = when
--    gpio.trig(echo, "down")
--  else
--    t_end = when
--  end
-- end

hcsr04 = {}
function hcsr04.init(pin_trig,pin_echo)
  local self={}
  self.time_start=0
  self.time_end=0
  self.trig=pin_trig or 4
  self.echo=pin_echo or 3
  gpio.mode(self.trig,gpio.OUTPUT)
  gpio.mode(self.echo,gpio.INT)
  function self.echo_cb(level, when)
    if level == 1 then
    self.time_start = when
    gpio.trig(self.echo, "down")
    else
    self.time_end = when
    gpio.trig(self.echo, "up")
    end
  end
  function self.measure()
    self.time_start=0
    self.time_end=0
    gpio.trig(self.echo, "up", self.echo_cb)
    gpio.write(self.trig, gpio.HIGH)
    tmr.delay(10)
    gpio.write(self.trig, gpio.LOW)
    tmr.delay(12000)
    if (self.time_end - self.time_start) < 0 then
      return -1
    end
    return (self.time_end - self.time_start) / 58
  end
  return self
end

sensor = hcsr04.init(1,2)
mi_alarm = tmr.create()
mi_alarm.register(1000, tmr.ALARM_AUTO, function()
  print(sensor.measure())
end)

mi_alarm.start()
