#!/bin/bash -e
#
# Brief:	Sube ejemplos .lua al nodemcu en modo solo de EJECUCIÓN
#     luego entra por Picocom para probar la respuesta en el monitor si así se requiere.
# Author: Johnny Cubides
# e-mail: jgcubidesc@gmail.com johnnycubides@catalejoplus.com
# date: Tuesday 05 February 2019

# foreground
BLACK=`tput setaf 0`
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
MAGENTA=`tput setaf 5`
CYAN=`tput setaf 6`
WHITE=`tput setaf 7`
NC=`tput setaf 7`
# background
BLACKB=`tput setab 0`
REDB=`tput setab 1`
GREENB=`tput setab 2`
YELLOWB=`tput setab 3`
BLUEB=`tput setab 4`
MAGENTAB=`tput setab 5`
CYANB=`tput setab 6`
WHITEB=`tput setab 7`
NCB=`tput setab 0`

SERIAL_TERM=picocom #picocom, minicom, screen
BAUD=115200 #9600
#view terminal
VT=y #y, n
#Port
PORT=/dev/ttyUSB0

#nodemcu-uploader --baud 115200 --port $3 exec $1 && picocom $3 --baud 115200
#nodemcu-uploader --baud 115200 --port $3 exec $1

function serial_com() {
  if [ "$SERIAL_TERM" = "picocom" ];
  then
    picocom $PORT --baud $BAUD
  fi
}

# Permite guardar información acerca de comandos usados
if [ "$1" = "-h" ] || [ "$1" = "" ] || [ "$1" = "--help" ];
then
    printf "Help for this command exec-example.sh\n"
    printf "\t ${CYAN}exec-example.sh${NC} ${MAGENTA}file.lua${NC} ${YELLOW}options${NC}\n"
    printf "\t[ ${YELLOW}Options${NC}]\n"
    printf "\t\tport\tSelect serial port, default /dev/ttyUSB0\n"
    printf "\t\tSerialTerm\tSelect if (y)->yes or (n) -> no, connect whith terminal, y\n"
    printf "\t\t-t PORT\tOpen serial port\n"
    printf "\t\t-d\tView dependencies\n"
    printf "\t\t-h,--help\tHelp\n"
    printf "\n\tExample:"
    printf "\t\texec-example.sh file.lua\n"
    printf "\t\texec-example.sh file.lua ${GREEN}y${NC}\n"
    printf "\t\texec-example.sh file.lua ${RED}n${NC} /dev/ttyUSB0\n"
    printf "\t\texec-example.sh file.lua /dev/ttyACM0\n"
    printf "\t\texec-example.sh file.lua /dev/ttyACM0 ${GREEN}y${NC}\n"
    printf "\t\texec-example.sh -t /dev/ttyACM0\n"
    printf "\t\texec-example.sh -t\n"
    printf "\nRegards Johnny.\n"
    # se verifica que el segundo argumento ($1) contenga la extensión lua
elif [ "$1" = "-d" ];
then
  printf "\tDependencias:\n"
  printf "\t\tnodemcu-uploader: https://github.com/kmpm/nodemcu-uploader.git\n"
  printf "\t\tpython-serial: apt install python-serial\n"
elif [ "${1##*.}" = "-t" ];
then
  if [ "${2%/*}" = "/dev" ];
  then
    PORT=$2
  fi
  serial_com
elif [ "${1##*.}" = "lua" ];
then
  if [ "$2" = "n" ];
  then
    VT=$2
    if [ "${3%/*}" = "/dev" ];
    then
      PORT=$3
    fi
  elif [ "${2%/*}" = "/dev" ];
  then
    PORT=$2
    if [ "$3" = "n" ];
    then
      VT=$3
    fi
  fi
  nodemcu-uploader --baud $BAUD --port $PORT exec $1
  if [ "$VT" = "y" ];
  then
    serial_com
  fi
fi
