// Listing 11.3 Simple OSC sender

// First, make an OSC send object and set port #
OscOut xmit;                           // (1) Makes an OSC output object.
6449 => int port;                      // (2) Port number to send OSC on.
xmit.dest ( "192.168.222.212", port );       // (3) Sets network destination to this computer.

// infinite time loop
while( true )                          // (4) Infinite loop.
{
    // start the message, after prefix (our choice)
    // expects one int, one float, one string
    /* xmit.start( "/chuck" );  // (5) Starts OSC output. */

    /* Math.random2(48,80) => int note;   // (6) Computes a random int note */
    /* Math.random2f(0.1,1.0) => float velocity; // (7) Computes a random float velocity */
    /* "Hi out there!!" => string message; // (8) Makes a string message */

    // a message is kicked as soon as it is complete
    // - type string is satisfied and bundles are closed
    /* note => xmit.add;                  // (9) Adds note to output. */
    /* velocity => xmit.add;              // (10) Adds velocity to output. */
    /* note => xmit.add;                  // (9) Adds note to output. */
    /* velocity => xmit.add;              // (10) Adds velocity to output. */
    /* message => xmit.add;               // (11) Adds string to output. */
    /* xmit.send();                       // (12) Send it! */ 
    /* xmit.start("chuck/timbre").add(0.1).add(0.22).add(0.44444).add(0.666666).send(); */
    xmit.start("chuck/timbre")
        .add(1)
        .add(256)
        .add(0.7777777)
        .add("off")
    .send();

    // hang a bit, then do it again
    1 :: second => now;
    <<<"test">>>;
}
