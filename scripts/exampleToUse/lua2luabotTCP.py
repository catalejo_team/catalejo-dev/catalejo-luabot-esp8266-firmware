#! /usr/bin/python
# -*- coding: utf-8 -*-

# python lua2luabotTCP.py file.lua delay(seconds) ip port

import sys
import socket

command = sys.argv[1] if len(sys.argv) > 1 else "none"
ip = sys.argv[2] if len(sys.argv) > 2 else "192.168.4.1"
port = int(sys.argv[3]) if len(sys.argv) > 3 else 1522
nameFile = sys.argv[4] if len(sys.argv) > 4 else "none"

s = socket.socket()


def save():
    s.connect((ip, port))
    s.send('save__begin\r')
    print(s.recv())
    with open(nameFile) as fp:
        for line in fp:
            print(line)
            s.send('save__data'+line+'\r')
            print(s.recv())
    s.send('save__end\r')
    print(s.recv())
    s.close()


def removeFile():
    s.connect((ip, port))
    s.send('save__rm\r')
    print(s.recv())
    s.close()


def main():
    if command == 'save':
        save()
    elif command == 'remove':
        removeFile()
    else:
        print('lua2luabotTCP.py command ip port file.lua')
        print('command[save, remove]')


main()
