--nodemcu como entrada midi para un pc
--use miditty para que sea reconicido como tal

gpio.mode(2, gpio.INPUT)
gpio.mode(1, gpio.INPUT)

ON = 144
OFF = 128

note = 0
state = 0

mi_alarma = tmr.create()
mi_alarma:register( 50, tmr.ALARM_AUTO, function()
  if gpio.read(2) == gpio.HIGH then
    mi_alarma:stop()
  end
  note = math.floor((127*adc.read(0))/1023) -- floot division
  if gpio.read(1) == gpio.HIGH then
    state = ON
  else
    state = OFF
  end
  uart.write(0, string.char(state), string.char(note), string.char(90))
end)

mi_alarma:start()
