# TEST PARA ESP32

No hay suficiente compatibilidad entre ambos módulos
esp8266 y el esp32, eso conlleva dificultades para la portación
en este punto es recomendable hacer uso de un RTOS para ambos
donde el interprete sea el puente y no se generen las dificultades
presentadas.

Las pruebas realizadas acá requieren de conda con python 3.7
para ello se creo una variable de entorno de conda denominada
*nodemcu-esp32*

Se ha creado el archivo `./luaUpload2esp32-serial.py` debido
a que la versión de lua tampoco soporta el manejo de file.open
sino hace uso del standar io.open de lua, lo cual hace que
herramientas como nodemcu-uploader, nodemcu-tools o explorer
fallen.

Se intentó instalar el docker de nodemcu para la compilación
pero también genera problemas, esto es lo que hace que se haga
uso de conda para poder instalar las dependiencias del constructor.

No todas las libreriías que se muestran en el siguiente comando funcionan
por eso se hace uso de conda
```bash
sudo apt-get install -y gperf python-pip python-dev flex bison build-essential libssl-dev libffi-dev libncurses5-dev libncursesw5-dev libreadline-dev cmake
```

Por tanto no tiene sentido continuar el desarrollo por este entorno.

Se recomienda el uso de zephyr e integrar Lua desde allí
