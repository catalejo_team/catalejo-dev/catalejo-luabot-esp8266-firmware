#! /usr/bin/python
# -*- coding: utf-8 -*-

# python myEspTool.py archivo.lua delay(seconds) pathTtyUsb 

import sys, serial, time

nameFile = sys.argv[1]
timeDelay = float(sys.argv[2])
nameUart = sys.argv[3]

uart  = serial.Serial(nameUart, 115200, timeout=1)

def main():
    uart.write(b';\n\r')
    time.sleep(0.5) # Delay for 200mS
    uart.write(str('file = io.open("' + nameFile + '", "w")\n\r').encode())
    time.sleep(0.5)
    print(uart.readline().decode("ascii"))
    with open(nameFile) as fp: 
        for line in fp:
            save_line = str("file:write([[\n\r" + line + "\r\n]])\n\r")
            for x in save_line:
                uart.write(x.encode())
                time.sleep(timeDelay)
            print("--TX--" + line + "--TX--")
            print("++RX++" + uart.readline().decode("ascii") + "++RX++")
            # for x in line:
            # uart.write(str("file:write([[\n\r" + line + "\r\n]])\n\r").encode())
            # time.sleep(timeDelay)
        uart.write(str.encode("file:close()\r\n"))
main()

