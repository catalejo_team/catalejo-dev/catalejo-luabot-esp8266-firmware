# Scripts/luabot

Contenido de éste directorio:

```bash
.
├── backup-node         # Archivos obtenidos desde el esp8266 con nodemcu-uploader
│   ├── apst.lc
│   ├── ds18b20.lc
│   ├── hcsr04.lc
│   ├── init.lua
│   ├── server.lc
│   └── wifi.lc
├── cross_files.sh      # Script para cross compilar archivos .lua a .lc sin ser aún probada en el esp8266
├── lc                  # Ejemplo de archivos crosscompilados sin ser probados en el esp8266
│   ├── apst.lc
│   ├── ds18b20.lc
│   ├── init.lc
│   ├── server.lc
│   └── wifi.lc
├── lua                 # Scripts Lua que responden al ambiente de catalejo-editor
│   ├── apst.lua        # Script wifi modo para conectarse a un AP y reset de run.lua
│   ├── ds18b20.lua     # Libreria para el uso del sensor digital de temperatura
│   ├── hcsr04.lua      # Libreria del sensor ultrasonido
│   ├── init.lua        # Script inicial que lanza los distintos modos
│   ├── server.lua      # Script responsable de la creación del run.lua
│   └── wifi.lua        # Scrip que configura el AP y la contraseña por default
└── README.md
```

## Observaciones

Tue Jan 17, 2023
Es importante cargar un run.lua con el blink en el LED de usuario D4 con nodemcu-uploader.
