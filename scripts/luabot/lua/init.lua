tmr.delay(1000000)
gpio.mode(4, gpio.OUTPUT)

function start()
	dofile("wifi.lc")
	gpio.mode(9, gpio.INPUT, gpio.PULLUP)
	gpio.mode(3, gpio.INPUT, gpio.PULLUP)
	gpio.write(4, gpio.LOW)
	if gpio.read(9) == 0 or gpio.read(3) == 0 then
		uart.setup(0, 115200, 8, 0, 1, 1)
		print("config ap-st")
		collectgarbage()
		dofile("apst.lc")
	else
		uart.setup(0, 115200, 8, 0, 1, 1)
		print("server.lc")
		collectgarbage()
		dofile("server.lc")
	end
end

start()
