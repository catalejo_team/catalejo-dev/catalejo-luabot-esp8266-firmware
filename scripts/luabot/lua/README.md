---
lang: es
---

# Scripts interprete Lua

Cuando se hace uso del comando make, make llamará a las reglas que se encuentran
dentro del archivo *makefile*, unas de las principales funciones de este archivo
son las reglas:

* mk-upload-folder: Realiza las crosscompilaciones desde lua y ubicar el el directorio *upload*
* upload-folder: Subir las crosscompilaciones al nodemcu.

## Probar HTTP peticiones

Esto permite observar si las peticiones están bien formadas.

```bash
curl -I ip:port
wget ip:port
```
