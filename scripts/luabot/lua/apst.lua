file.remove("run.lua")
gpio.write(4, gpio.LOW)
local str = wifi.ap.getmac()
local ssidTemp = string.format("%s%s", string.sub(str, 13, 14), string.sub(str, 16, 17))
cfg = {}
cfg.ssid = "catalejo-" .. ssidTemp
cfg.pwd = "87654321"
cfg.save = true
local oldChannel = wifi.ap.getconfig(true).channel
local channels = { 1, 3, 6, 11, 13 }
local newChannel = 1
if oldChannel == 13 then
	newChannel = 1
else
	for _, channel in pairs(channels) do
		newChannel = channel
		if newChannel > oldChannel then
			break
		end
	end
end
cfg.channel = newChannel
wifi.setmode(wifi.SOFTAP)
wifi.ap.config(cfg)

collectgarbage()
gpio.write(4, gpio.HIGH)

ledStatusSTA = tmr.create()
ledStatusSTA:register(1000, tmr.ALARM_AUTO, function()
	if wifi.sta.status() == wifi.STA_GOTIP then
		gpio.write(4, gpio.LOW)
		ledStatusSTA:stop()
	end
end)

function connect2AP(ssid, pass)
	-- wifi.setmode(wifi.STATION)
	wifi.setmode(wifi.STATIONAP)
	ledStatusSTA:stop()
	station_cfg = {}
	station_cfg.ssid = ssid
	station_cfg.pwd = pass
	station_cfg.save = true
	station_cfg.auto = true
	wifi.sta.config(station_cfg)
	-- wifi.sta.connect()
	ledStatusSTA:start()
end

srv = net.createServer(net.TCP, 60)
srv:listen(80, function(conn)
	conn:on("receive", function(conn, payload)
		ssid_start, ssid_end = string.find(payload, "SSID=")
		if ssid_start and ssid_end then
			amper1_start, amper1_end = string.find(payload, "&", ssid_end + 1)
			if amper1_start and amper1_end then
				http_start, http_end = string.find(payload, "HTTP/1.1", ssid_end + 1)
				if http_start and http_end then
					ssid = string.gsub(string.sub(payload, ssid_end + 1, amper1_start - 1), "+", " ")
					password = string.gsub(string.sub(payload, amper1_end + 10, http_start - 2), "+", " ")
					if ssid == "" then
						wifi.sta.clearconfig()
						print("STA config deleted")
					elseif ssid and password then
						connect2AP(ssid, password)
					end
				end
			end
		end
	end)
	local remaining, used, total = file.fsinfo()
	local l = file.list()
	local fileAndSize = ""
	for k, v in pairs(l) do
		fileAndSize = fileAndSize .. "<h4>name: " .. k .. "\t, size: " .. v .. " Bytes</h4>\r\n"
	end
	local body = "<!DOCTYPE html>\r\n"
		.. "<html>\r\n"
		.. "<head>\r\n"
		.. '<meta name="viewport" charset="utf-8" content="width=device-width, initial-scale=1">\r\n'
		.. "</head>\r\n"
		.. "<body>\r\n"
		.. "<h1>Luna </h1>\r\n"
		.. "<h2>Conectarse a una red</h2>\r\n"
		.. "<h4>Introducir el SSID y password de la red</h4>\r\n"
		.. "<form action='' method='get'>\r\n"
		.. "<label for='SSID'>SSID red  : </label>"
		.. "<input type='text' name='SSID' value='' maxlength='100'/>\r\n"
		.. "<br/>"
		.. "<label for='Password'>Password: </label>"
		.. "<input type='text' name='Password' value='' maxlength='100'/>\r\n"
		.. "<input type='submit' value='Submit' />\r\n"
		.. "</form>\r\n"
		.. "<p>Nota: Si deja el SSID de red vacío se borrará la configuración de red anterior</p>\r\n"
		.. "<p>IP en red LAN: "
		.. (wifi.sta.getip() and "<b>" .. wifi.sta.getip() .. "</b>" or "no conectado a una red.")
		.. "</p>\r\n"
		.. "<h3>File system info:</h3>\r\n"
		.. "<h4>Total: "
		.. total
		.. " Bytes, Used: "
		.. used
		.. " Bytes, Remain: "
		.. remaining
		.. " Bytes\r\n</h4>"
		.. "<h3>Lists all files in the file system</h3>\r\n"
		.. fileAndSize
		.. "<h4>MAC: "
		.. wifi.ap.getmac()
		.. "</h4>\r\n"
		.. "<h2>www.catalejoplus.com</h2>"
		.. "</body>\r\n"
		.. "</html>\r\n"
	local pageWifi = "HTTP/1.1 200 OK\r\n"
		.. "Connection: Keep-Alive\r\n"
		.. "Content-Length: "
		.. string.len(body)
		.. "\r\n"
		.. "Content-Type: text/html; \r\n\r\n"
		.. body
	conn:send(pageWifi)
end)
