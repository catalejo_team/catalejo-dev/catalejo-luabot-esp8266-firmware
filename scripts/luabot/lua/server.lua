gpio.mode(4, gpio.OUTPUT)
counter = 0
function saveLua(data2save)
  if string.match(data2save,'^.-data')=="save__data" then
    local data_save = string.sub(data2save,11)
    file.write(data_save)
    gpio.write(4,gpio.HIGH)
  elseif string.match(data2save,'^.-begin')=="save__begin" then
    file.remove("run.lua")
    file.open("run.lua","w")
    file.write([[""]])
    file.close()
    file.open("run.lua","w+")
    gpio.write(4,gpio.HIGH)
  elseif string.match(data2save,'^.-end')=="save__end" then
    file.close()
    gpio.write(4,gpio.HIGH)
  elseif string.match(data2save,'^.-rm')=="save__rm" then
    file.remove("run.lua")
    gpio.write(4,gpio.HIGH)
  elseif string.match(data2save,'^.-rs')=="save__rs" then
    node.restart()
  else
    file.close()
  end
end

sv1522 = net.createServer(net.TCP, 30)

function receiver1522(sck, data)
  gpio.mode(4, gpio.OUTPUT)
	gpio.write(4,gpio.LOW)
  saveLua(data)
  counter= counter + 1
  sck:send(counter.."\r\n")
  --sck:close()
end
if sv1522 then
  sv1522:listen(1522, function(conn)
    conn:on("receive", receiver1522)
    -- conn:send("ok\r\n")
    conn:on("connection", function(sck)
      counter = 0
      sck:send("luabot,"..(wifi.sta.getip() and wifi.sta.getip() or "no_sta").." \r\n")
      -- sck:send("luabot\r\n")
    end)
  end)
end

if file.exists('run.lua') then
  print("run")
  collectgarbage()
  dofile('run.lua')
end
