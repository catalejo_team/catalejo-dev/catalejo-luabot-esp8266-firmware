# catalejo-luabot-esp8266-firmware

**Este proyecto no puede ser borrado ya que existen varios proyecto que dependen de este repositorio**.

Firmware basado en nodemcu lua para el esp8266.

Releases:

Jan 18, 2023: El run.lua puede ser borrado colocando un 0 lógico en el pin RX o en el gpio0, el gpio0 es el pin D3 en la tarjeta de desarrollo nodemcu v3.

Johnny Cubides

jgcubidesc@gmail.com
